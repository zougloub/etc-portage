import sys, io, os
import logging
import socket

import socks

logger = logging.getLogger()

class UnixSocket(socket.socket):
	def __init__(self, *pos, **kw):
		socket.socket.__init__(self, #*pos, **kw)
		 socket.AF_UNIX, socket.SOCK_STREAM)
		logger.debug("Init")

def connect(self, addr):
	logger.debug("Connect %s %s", self, addr)
	ret = socket.socket.connect(self, addr)
	logger.debug("Connected: %s", self)
	return ret

class MySockSocket(socks.socksocket):
	def __init__(self):
		socks.socksocket.__init__(self)
		self.proxy = socks.SOCKS5, 2, 3, 4, None, None

	def _proxy_addr(self):
		ret = os.environ["DISTCC_SOCKS_PROXY"]
		return ret

socks._BaseSocket.connect = connect
socks._orig_socket = UnixSocket

def test():
	s = MySockSocket()

	s.connect(("exmakhina.com", 80))
	logger.debug("Connected %s", s)
	s.sendall(b"GET / HTTP/1.1\r\n\r\n")
	logger.debug("Sent")
	data = s.recv(4096)
	logger.debug("Data: %s", data)
	sys.stderr.write("\x1B[33;1mpouet!\x1B[0m\n")

def main():
	import argparse

	parser = argparse.ArgumentParser(
	 description="Executor stations monitor test",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "test",
	 help="Run tests",
	)

	def do_test(args):
		test()

	subp.set_defaults(func=do_test)


	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 level=getattr(logging, args.log_level),
	 format="%(pathname)s:%(lineno)s %(levelname)s %(message)s"
	)


	if getattr(args, 'func', None) is None:
		parser.print_help()
		return 1
	else:
		args.func(args)
		return 0

if __name__ == "__main__":
	res = main()
	raise SystemExit(res)

