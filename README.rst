#####################
Portage Configuration
#####################

Intro
#####

This configuration allows tracking of multiple computers
with various profiles (embedded, server, desktop, developer),
in a shared git repository.

Features
########

#. SCM-managed configuration

   Enables centralized administration of machines.

#. Quick installation of an environment

#. Centralized (or not) administration of machines is possible

#. Management of system configuration via configured sets

   Sets allow to install all the necessary interesting packages on a system.
   A bunch of configured sets are inside the ``sets/`` folder.


Installing
##########

Install the desired sets.
After that, it's all about maintenance.


Managing a machine configuration
********************************

This installation manages the paludis configuration, which consists of
the following files:

- ``repos.conf/``: repositories configurations

  - ``gentoo.conf``: defines the gentoo (main) repository, along with
    the machine profile (basic pre-customized distribution available
    in upstream Gentoo).
    Generated file!

- ``package.use``: profile overrides package dependency configurations.
  Generated file!

- ``package.keywords``: profile overrides for accepted "keywords"
  Generated file!

- ``package.mask``, ``package.unmask``: profile overrides
  for masked and unmasked packages.
  Generated files!

- ``bashrc``: overrides the environment when executing build commands (Gentoo runs
  bash scripts).

The files are generated from a dynamic configuration, depending on the
machine, CPU features, and packages that are already installed.

See the ``genconfig`` folder for the generator code.
Either use a predefined configuration by writing a configured hostname
(see the various ``host_in`` invocations in ``genconfig/*.sh``, ``bashrc``)
 in ``/etc/portage/hostname``.
For instance, a company computer will have a variable hostname, but
can use a standard configuration.

Deviate from a configuration by editing ``functions.sh`` to consider a
machine as member of a group, and now you can refer to it by its host
name or the group name.


Using distcc and ccache
***********************

This configuration includes usage of distcc, using a shared
distcc setup.
To install this distcc profile:

.. code:: bash

  groupadd distcc
  gpasswd -a portage distcc
  mkdir -p /var/cache/distcc/{lock,state,tmp}
  echo 192.168.0.119/3,lzo > /var/cache/distcc/hosts
  echo localhost/2 >> /var/cache/distcc/hosts
  chown -R portage:distcc /var/cache/distcc

Then create the distcc symlinks in /usr/libexec/distcc/bin using:

.. code:: bash

  /etc/portage/scripts/fix_ccache





Maintenance
###########

If you're not me, feel free to commit and push in development
branches on the repo. I might like your ideas.



Bootstrap
#########

The world can't be installed in one shot, bootstrap is needed.
If you copy world from one machine to an empty one, don't even try
to install world.


Gentoo Maintenance Tips
#######################


Fixing Linkage (aka reverse dependencies)
*****************************************

Gentoo handles most of the linkage updates via the packages EAPI,
but sometimes there are still issues, so the following can help make
sure that the system is well tied together.

C ABI linkage is handled by TODO:

Perl modules can be rebuilt for the installed versions of perl with:

.. code:: bash

   perl-cleaner --modules -- --resume-file /root/cave-perl

Python modules can be rebuilt with this tool:

.. code:: bash

   python-updater -- --resume-file /root/cave-python

Haskell modules can be rebuilt with:

.. code:: bash

   haskell-updater -- --resume-file /root/cave-haskell

Modules from other languages can be rebuilt with:

.. code:: bash

   /etc/portage/scripts/fix-linkage-scripts $lang


Interpreter Upgrades
********************

- Python

  - Check if interesting packages exist for the new interpreter

    .. code:: sh

       TODO


Deployment
##########

See ``scripts/stdlinux.py``.
