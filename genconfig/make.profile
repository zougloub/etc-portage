#!/bin/bash
# Generate location of make.profile symlink

cwd=$(dirname $0)

. $cwd/functions.bash

if host_in pouet Epic; then
	echo default/linux/amd64/17.1
else
	echo default/linux/amd64/17.0
fi