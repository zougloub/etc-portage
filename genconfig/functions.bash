#!/bin/bash

hostname_fn() { echo "/etc/portage/hostname"; }

is_host() {
	local host=$(uname -n | sed -r 's/(.*?)\..*$/\1/')

	if [ -e "$(hostname_fn)" ]; then
		host="$(cat "$(hostname_fn)")"
	fi

	if [ "$1" == "opencl" ]; then
		case $host in
			Niwatori|Vantage|Cabochon|Bidule|zougloub)
			return 0;
			;;
			*)
			return 1;
			;;
		esac
	fi

	if [ "$1" == "libressl" ]; then
		return 1
	fi

	if [ "$1" == "numa" ]; then
		case $host in
			Epic)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "docker" ]; then
		case $host in
			MacGyver)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "CUDA" ]; then
		case $host in
			Niwatori|pouet|Epic)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "proprietary-nvidia" ]; then
		case $host in
			Niwatori|pouet|Epic)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "server" -a "$host" == "MacGyver" ]; then
		return 0;
	fi
	if [ "$1" == "server" -a "$host" == "zougloub" ]; then
		return 0;
	fi
	if [ "$1" == "server" -a "$host" == "ForYou" ]; then
		return 0;
	fi

	if [ "$1" == "server" -a "$host" == "Epic" ]; then
		return 0;
	fi

	if [ "$1" == "ExMakhina" ]; then
		case $host in
			Cabochon|Bidule|Guinness|Vantage|pouet)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "smp" ]; then
		case $host in
			Vermine|Mono|MacGyver|waspberry)
			return 1;
			;;
			*)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "ltj" ]; then
		case $host in
			LTJ-*)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "TamaggoWS" ]; then
		case $host in
			LTJ-*|jpoitras)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "minimal" ]; then
		case $host in
			Momo|Vermine|MacGyver|waspberry)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "cJ_dev" ]; then
		case $host in
			Vantage|Bidule|Cabochon|pouet)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "printer" ]; then
		case $host in
			Vantage|pouet)
			return 0;
			;;
		esac
	fi

	if [ "$1" == "$host" ]; then
		return 0;
	fi

	return 1
}

host_in() {
	local host
	for host in $*;
	do
		if is_host $host; then
			return 0;
		fi
	done
	return 1;
}


installed() {
	#ls /var/db/pkg/${1}* > /dev/null  2>/dev/null
	equery list "${1}" >/dev/null 2>&1
	return $?
}
