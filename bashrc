#!/bin/bash
# Portage bashrc file
#
# This files overrides default environment variables that are used
# during the build process of every package
#
# Interesting variables include PATH, as well as build tools
# variables to set the amount of debugging information, configure
# parallel building and other stuff like that.


case $EBUILD_PHASE in
	variable)
	# Use to read a variable from an environment file
	;;

	depend)
	# portage
	;;

	*)
	echo "Sourcing bashrc in phase $EBUILD_PHASE for ${CATEGORY}/${P}" >&2
	echo "umask: $(umask), id: $(id)" >&2
	;;
esac


cJ_env() {
	printf " [32;1m*[0m I am %s\n" "$(id)" >&2
	printf " [32;1m*[0m CC=%s (%s)\n" "${CC}" "$(which "${CC}")" >&2
	printf " [32;1m*[0m PATH=%s\n" "${PATH}" >&2
	printf " [32;1m*[0m HOME=%s\n" "${HOME}" >&2
	printf " [32;1m*[0m CCACHE vars: %s\n" "${!CCACHE_*}" >&2
	for var in ${!CCACHE_*}; do
		printf " [32;1m*[0m %s=%s\n" "${var}" "$(eval echo \$${var})" >&2
	done
	printf " [32;1m*[0m DISTCC vars: %s\n" "${!DISTCC_*}" >&2
	for var in ${!DISTCC_*}; do
		printf " [32;1m*[0m %s=%s\n" "${var}" "$(eval echo \$${var})" >&2
	done

	env \
	 | egrep "(^(C(FLAGS|C|XX)|C(HOST|BUILD|TARGET)|MAKEOPTS|EBUILD_PHASE|CCACHE|DISTCC|DCC|HOOK|SANDBOX)|ABI|MULTILIB)" \
	 | sort \
	 | grep -v cJ_env \
	 | perl -pe 's/(.*)/  $1/g' \
	 >&2
}


cwd=/etc/portage
#$(dirname $0)
. $cwd/genconfig/functions.bash


cJ_setup_compress() {
	PORTAGE_COMPRESS="xz"
	PORTAGE_COMPRESS_FLAGS="--memory=max --extreme"
}

SKIP_FUNCTIONS="test"
# Use portage credentials for fetchers
#SVN_WRAPPER="/etc/paludis/sudo"
#GIT_WRAPPER="/etc/paludis/sudo"
#RSYNC_WRAPPER="/etc/paludis/sudo"
#WGET_WRAPPER="sudo -u portage"
#WGET_WRAPPER="busybox"
EXTRA_WGET="--no-check-certificate"



umask 0002

cJ_setup_kernel_dirs() {
	case $EBUILD_PHASE in
		pretend)
		;;
		init)
			KBUILD_OUTPUT=/usr/src/linux
			# Kernel build location
			if host_in zougloub Potiok TamaggoWS AlgoluxWS Bidule Cabochon ForYou Niwatori; then
				KBUILD_OUTPUT=/var/linux
			elif host_in Bidule Cabochon; then
				KBUILD_OUTPUT=/mnt/var/linux
			fi
			;;
	esac
	cJ_workaround_linux_info
	echo "KV_FULL: $KV_FULL" >&2
}




cJ_workaround_linux_info() {
	# Workaround linux-info not getting kernel version right
	# https://bugs.gentoo.org/87242

	KV_OUT_DIR="${KBUILD_OUTPUT}"
	KERNEL_DIR=$(readlink -f "${KBUILD_OUTPUT}/source")
	KV_DIR="${KERNEL_DIR}"
	KV_FULL="$(<"${KBUILD_OUTPUT}"/include/config/kernel.release)"
	KV_LOCAL="$(<"${KBUILD_OUTPUT}"/include/config/kernel.release)"
	KERNEL_MAKEFILE="${KV_DIR}/Makefile"

	cJ_getfilevar_noexec() {
		cat "${2}" | sed -n \
						 -e "/^[[:space:]]*${1}[[:space:]]*:\\?=[[:space:]]*\(.*\)\$/{
			s,^[^=]*[[:space:]]*=[[:space:]]*,,g ;
			s,[[:space:]]*\$,,g ;
			p
		}"

	}

	KV_MAJOR=$(cJ_getfilevar_noexec VERSION "${KERNEL_MAKEFILE}")
	KV_MINOR=$(cJ_getfilevar_noexec PATCHLEVEL "${KERNEL_MAKEFILE}")
	KV_PATCH=$(cJ_getfilevar_noexec SUBLEVEL "${KERNEL_MAKEFILE}")
	KV_EXTRA=$(cJ_getfilevar_noexec EXTRAVERSION "${KERNEL_MAKEFILE}")
}


cflags_get_default() {
	if host_in AlgoluxWS; then
		echo " -march=core2 "
		return
	fi
	#echo $CC >&2
	local guessed=$($CC -march=native -E -v - </dev/null 2>&1 | grep cc1)
	#echo guessed $guessed >&2
	local march=${guessed//*march=/}
	march=${march// */}
	local default=$($CC -march=$march -E -v - </dev/null 2>&1 | grep cc1)
	#echo default $default >&2
	guessed=${guessed// -m64/}
	default=${default// -m64/}
	guessed=${guessed// -m32/}
	default=${default// -m32/}
	guessed=${guessed// -fstack-protector-strong/}
	default=${default// -fstack-protector-strong/}
	guessed=${guessed// -fstack-protector/}
	default=${default// -fstack-protector/}
	#echo $default >&2
	#echo $guessed >&2
	local flags="-march=$march ${guessed#$default}"
	flags="${flags//  / }"
	#echo $flags >&2
	echo $flags
}

cJ_set_extra() {
	case ${CATEGORY}/${P} in
		dev-libs/opensc-*)
		EXTRA_ECONF=" --disable-strict"
		;;

		media-libs/gst-plugins-bad-*)
		if host_in CUDA; then
			EXTRA_ECONF+=" --enable-nvenc"
		fi
		;;

		media-libs/gst-plugins-good-*)
		EXTRA_ECONF+=" --enable-qt"
		;;

		app-admin/sudo-*)
		case $host in
			zPotiok)
			EXTRA_ECONF=" --with-runas-default=pouet"
			;;
		esac
		;;

		*/httptunnel-*)
			export EXTRA_ECONF="  --enable-debug"
		;;

		app-arch/tar-*)
		case $host in
			Potiok)
			EXTRA_ECONF=" --with-bzip2=lbzip2"
			;;
		esac
		;;

		net-misc/httptunnel-*)
		case $host in
			Potiok)
			EXTRA_ECONF=" --enable-debug"
			;;
		esac
		;;
		net-misc/openvpn-2*)
		EXTRA_ECONF=" --enable-password-save"
		;;

		sys-devel/llvm-3.5*)
		if host_in AlgoluxWS; then
			EXTRA_ECONF=" --enable-targets=cpp,x86_64,arm,arm64,nvptx"
		fi
		;;

		*)
		;;
	esac

	if [ "z$PALUDIS_USE_CFGCACHE" = "z1" ]; then
		local cfgcache=/var/tmp/paludis/configure.cache
		sed -i \
		 -e 's/.*CFLAGS.*//g' \
		 -e 's/.*CPPFLAGS.*//g' \
		 -e 's/.*CXXFLAGS.*//g' \
		 -e 's/.*LDFLAGS.*//g' \
		 $cfgcache
		EXTRA_ECONF="-C --cache-file=$cfgcache"
	fi

	if [ -n "$EXTRA_ECONF" ];
	then
		elog "Using special ECONF for $PN[0m: $EXTRA_ECONF"
	fi
}


cJ_default_chost() {
	export CHOST=x86_64-pc-linux-gnu
	if host_in jiji Vermine Zerg Momo; then
		CHOST=i686-pc-linux-gnu
	elif host_in waspberry; then
		CHOST=armv6j-hardfloat-linux-gnueabi
	fi
}

cJ_set_cflags() {
	elog "CC=${CC} CXX=${CXX} CHOST=${CHOST}"

	if [ -z $CHOST ]; then
		cJ_default_chost
	fi

	case ${CATEGORY}/${PN} in
		cross-*)
			case ${CATEGORY}/${PN} in
				*binutils*)
				;;
				*gcc*)
				;;
				*linux-headers*)
				;;
				#*glibc*)
				#;;
				#*mingw64-runtime*)
				#;;
				*)
				export CHOST=${CATEGORY#cross-}
				;;
			esac
		;;

		*)
		cJ_default_chost
		;;
	esac

	CC=${CHOST}-gcc
	CXX=${CHOST}-g++

	# The mingw64-runtime ebuild is expecting a non-cross-compile CHOST
	# 
	case ${CATEGORY}/${PN} in
		cross*mingw64-runtime*)
		cJ_default_chost
		;;
	esac

	elog "CC=${CC} CXX=${CXX} CHOST=${CHOST}"

	for i in CC CXX; do
		export $i
		if [ -n "$(eval echo \$${i}_PALUDIS)" ]; then
			elog "Appending provided $i: $(eval echo \$${i}_PALUDIS)"
			local "${i}_BAK=$(eval echo \$${i}_PALUDIS)"
		else
			local "${i}_BAK="
		fi
		local "${i}_DEF=$(eval echo \$${i})"
	done

	# Compiler deviations
	case ${CATEGORY}/${P} in
		dev-util/nvidia-cuda-*)
		:
		;;

		www-client/chromium)
		CC="clang"
		CXX="clang++"
		;;

		net-fs/coda-*)
		CC="${CHOST}-gcc-3.3.6"
		;;

		zdev-python/PyQt5-*)
		# function cast issue
		CC="${CHOST}-gcc-7.3.0"
		CXX="${CHOST}-g++-7.3.0"
		;;

	esac

	# *FLAGS setup

	if host_in waspberry; then
		CFLAGS="-pipe -march=armv6j -mfpu=vfp -mfloat-abi=hard -Os"
		CXXFLAGS="$CFLAGS"
	elif host_in other; then
		CFLAGS=" -pipe -Os"
		CXXFLAGS=" -pipe -Os"
	else
		CFLAGS=" -pipe $(cflags_get_default) -Os"
		CXXFLAGS=" -pipe $(cflags_get_default) -Os"
	fi

	LDFLAGS="-Wl,--as-needed -Wl,--hash-style=gnu -Wl,--sort-common"

	for i in CC CXX CFLAGS CXXFLAGS LDFLAGS CHOST CBUILD CTARGET;
	do
		export $i
		if [ -n "$(eval echo \$${i}_PALUDIS)" ]; then
			elog "Appending provided $i: $(eval echo \$${i}_PALUDIS)"
			local "${i}_BAK=$(eval echo \$${i}_PALUDIS)"
		else
			local "${i}_BAK="
		fi
		local "${i}_DEF=$(eval echo \$${i})"
	done

	#VERSION=$($CC  -v 2>&1 | grep "^gcc version" | awk '{ print $3 }')

	# Determining what -march=$march really does
	#   echo "int main() { return 0; }" | gcc -march=$(cflags_get_default) -v -Q -x c - 2>&1
	#   gcc -Q --help=target -march=native
	#   echo | gcc -dM -E - -march=native
	# We don't use march=native directly, it's bad for distcc


	####
	# CFLAGS deviations

	#debug
	#OCFLAGS="${CFLAGS} -Wall -Wextra -Wconversion -Wunreachable-code -Wpointer-arith -Wlarger-than-3000 -Wfloat-equal -pedantic -ansi"
	#OCFLAGS="${CFLAGS} -ftree-vectorizer-verbose=1"
	#CXXFLAGS="${CFLAGS}"
	#CPPFLAGS="${CFLAGS} -Weffc++"

	if host_in Bidule Cabochon; then
		#CFLAGS="$CFLAGS -fschedule-insns -fsched-pressure"
		#CXXFLAGS="$CXXFLAGS -fschedule-insns -fsched-pressure"
		:
	fi
	if host_in Potiok Malcolm Vermine; then
		CFLAGS+=" -fomit-frame-pointer"
		CXXFLAGS+=" -fomit-frame-pointer"
	fi

	# per package
	### Compiler deviations
	# careful: -r${revision} is not in ${P}
	case ${CATEGORY}/${P} in
		#dev-db/sqlite)
		#elog "Exporting SQLite3 internals"
		#CFLAGS="$CFLAGS -DSQLITE_PRIVATE=\"\""
		#CXXFLAGS="$CXXFLAGS -DSQLITE_PRIVATE=\"\""
		#;;

		app-text/ttf2pk2-*)
		CFLAGS+=" -I/usr/include/freetype2"
		LIBS="${LIBS} -lfreetype"
		LDFLAGS+=" -lfreetype"
		;;

		app-text/tesseract-*)
		LDFLAGS+=" -lOpenCL"
		;;

		dev-libs/libgcrypt-*)
		CFLAGS="-O2"
		;;

		#dev-embedded/urjtag-*)
		#LDFLAGS+=" -lm"
		#;;

		dev-python/pivy-*)
		# fails compilation
		# /usr/include/coin/Inventor/SbBasic.h:99:5: error: 'cc_debugerror_post' was not declared in this scope,
		# and no declarations were found by argument-dependent lookup at the point of instantiation [-fpermissive]
		CFLAGS+=" -fpermissive"
		CXXFLAGS+=" -fpermissive"
		;;

		dev-util/nvidia-cuda-*)
		CFLAGS="-pipe -Os"
		CXXFLAGS="-pipe -Os"
		;;

		#sys-devel/make-*)
		#EPATCH_EXCLUDE=make-3.82-intermediate-parallel.patch
		#CFLAGS+=" -O3 -flto"
		#LDFLAGS+=" -O3 -flto"
		#;;

		dev-qt/qtcore-*)
		# https://bugs.kde.org/show_bug.cgi?id=396176
		CFLAGS="$CFLAGS -g -mno-rdrnd"
		CXXFLAGS="$CXXFLAGS -g -mno-rdrnd"
		elog "Using -g in C*FLAGS"
		;;

		dev-qt/qtnetwork-*)
		# fails compilation
		# ssl/qsslsocket_openssl_symbols.cpp:193:49: error: invalid conversion from ‘const BIO_METHOD*’ {aka ‘const bio_method_st*’} to ‘BIO_METHOD*’ {aka ‘bio_method_st*’} [-fpermissive]
		CFLAGS+=" -fpermissive"
		CXXFLAGS+=" -fpermissive"
		;;

		sys-apps/systemd*|*/omniORB*|dev-db/sqlite*|*/python*|*/gtk+*|*/glib*|*/wxGTK*|*/wxpython*|*/pygobject*|*/darktable*|*/opencv*|mail-client/claws-mail-*)
		CFLAGS="$CFLAGS -g"
		CXXFLAGS="$CXXFLAGS -g"
		elog "Using -g in C*FLAGS"
		;;

		dev-lang/gnat-gcc-*)
		CFLAGS=""
		CXXFLAGS=""
		;;

		net-ftp/filezilla-*)
		CXXFLAGS+=" -fpermissive"
		;;

		dev-lang/halide-*)
		CFLAGS+=" -DWITH_EXCEPTIONS"
		CXXFLAGS+=" -DWITH_EXCEPTIONS"
		;;

		media-libs/libomxil-bellagio-*)
		CXXFLAGS+=" -Wno-error"
		CFLAGS+=" -Wno-error"
		;;

		net-libs/usrsctp-*)
		CXXFLAGS+=" -Wno-error"
		CFLAGS+=" -Wno-error"
		;;


		sci-libs/clblas-*)
		LDFLAGS+=" -lpthread"
		;;

		sys-apps/fwupdate-*):
		CFLAGS+=" -Wno-error=address-of-packed-member"
		;;


		sys-apps/paludis-*)
		#CXXFLAGS+=" -O3" # never do that...
		CXXFLAGS+=" -g -D_GLIBCXX_USE_CXX11_ABI=0" # http://paludis.exherbo.org/trac/ticket/1335#comment:1
		;;

		sys-devel/gcc-*)
		#CFLAGS="-Os"
		#CXXFLAGS="-Os"
		:
		;;

		sys-libs/glibc-*)
		elog "Using glibc debug info (gdb heap)"
		CFLAGS+=" -g"
		CXXFLAGS+=" -g"
		;;

		sys-libs/libstdc++-v3-*) # cross gcc compilation
		CFLAGS="-Os"
		CXXFLAGS="-Os"
		;;

		sys-devel/llvm-3.3*)
		CFLAGS=" -pipe $(cflags_get_default) -Os"
		CXXFLAGS=" -pipe $(cflags_get_default) -Os"
		;;

		sys-devel/llvm-gcc-*)
		# as of 2011-11-23, does not accept complex CFLAGS when xgcc'ing
		CFLAGS="-O2"
		CXXFLAGS="-O2"
		;;

		games-*/*)
		# games don't care about FPU accuracy
		CFLAGS+=" -ffast-math"
		;;

		net-misc/tor-*)
		# https://trac.torproject.org/projects/tor/ticket/10259
		# http://gcc.gnu.org/bugzilla/show_bug.cgi?id=59358
		CFLAGS=" -pipe $(cflags_get_default) -O1"
		CXXFLAGS=" -pipe $(cflags_get_default) -O1"
		;;

		#dev-libs/glib|sys-libs/ncurses|sys-libs/zlib|media-libs/libjpeg-turbo)
		# Options for qemu --static
		#CFLAGS="-Os"
		#CXXFLAGS="-Os"
		#;;

		sci-electronics/ghdl-*)
		CFLAGS="-Os"
		CXXFLAGS="Os"
		;;

	esac

	### Cross-compilation
	# other potential vars: HOSTCC/HOSTCFLAGS
	case z${CATEGORY}/${PN} in
		cross-*/binutils)
		unset CC
		unset CXX
		unset CFLAGS
		unset CXXFLAGS
		unset CBUILD
		unset CTARGET
		elog "reset C{C,XX,FLAGS,XXFLAGS,BUILD,TARGET}"
		;;

		cross-*)
		#unset CC
		#unset CXX
		#unset CFLAGS
		#unset CXXFLAGS
		unset CHOST
		unset CBUILD
		unset CTARGET
		elog "reset C{C,XX,FLAGS,XXFLAGS,HOST,BUILD,TARGET}"
		;;
	esac

	CFLAGS+=" $CFLAGS_BAK"
	CXXFLAGS+=" $CXXFLAGS_BAK"

	for i in CC CXX CFLAGS CXXFLAGS LDFLAGS CHOST CBUILD CTARGET;
	do
		if [ "$(eval echo \$${i})" != "$(eval echo \$${i}_DEF)" ]; then
			elog "Using special $i: $(eval echo \$${i})"
			# git diff --word-diff --no-index a b
		fi
	done

}

get_jobs() {
	local NCPUS=$(cat /proc/cpuinfo | grep processor | awk '{a++} END {print a}')
	local JOBS=$((NCPUS+NCPUS/4))
	echo $JOBS
}

cJ_set_jobs() {
	export JOBS=$(get_jobs)

	case ${CATEGORY}/${PN} in
		#cross-*/*)
		#;;

		*)
		echo -ne " [32;1m*[0m Enabling ccache/distcc if possible (init. jobs: $JOBS)..." >&2
		file=/etc/portage/ccache_distcc.env
		#file=/usr/local/bin/ccache+distcc.env

		if ip link | grep "state UP" >/dev/null; then
			USE_DISTCC=1
		else
			printf " [32;1m*[0m No distcc" >&2
			USE_DISTCC=0
		fi

		if host_in Bidule Cabochon Vantage pouet; then
			USE_CCACHE=1
			unset CCACHE_DISABLE
		else
			printf " [32;1m*[0m No ccache" >&2
			USE_CCACHE=0
		fi

		#USE_CCACHE=0
		#USE_DISTCC=0
		printf " [32;1m*[0m Enabling ccache %s distcc %s\n" "${USE_CCACHE}" "${USE_DISTCC}" >&2

		if [ -e $file ]; then
			source $file
		fi

		if [ $USE_CCACHE = 1 ]; then
			SANDBOX_WRITE="${SANDBOX_WRITE}:${CCACHE_DIR}"
		fi

		if [ $USE_DISTCC = 1 ]; then
			SANDBOX_WRITE="${SANDBOX_WRITE}:${DISTCC_DIR}"
			MOREJOBS=0
			distcc_hosts="$DISTCC_DIR/hosts"

			if [ -f $distcc_hosts ]; then
				while read host num;
				do
					if [ -z $num ];
					then
						num=0
					fi
					case $host in
						*)
							echo -n "($host/$num? " >&2
							ping -c 1 -w 1 -W 1 $host >/dev/null 2>/dev/null
							if [ $? == 0 ];
							then
								MOREJOBS=$(($MOREJOBS + $num))
								echo -n "+$num : $MOREJOBS)" >&2
							else
								echo -n "timeout)" >&2
							fi

						;;
					esac
				done < <(cat $distcc_hosts | perl -ne 'if (/^[^#]/ && /^(?<host>\d+\.\d+\.\d+\.\d+|\S+?)(\/(?<num>\d+))?(,lzo)?$/) { print "$+{host} $+{num}\n"; }')
			fi
			if [ $MOREJOBS -gt $JOBS ];
			then
				echo " OK: distcc changed JOBS from $JOBS to $MOREJOBS" >&2
				JOBS=$MOREJOBS
			fi
		else
			echo " not enabled" >&2
		fi
		;;
	esac

	case ${CATEGORY}/${PN} in

		app-misc/screen)
		export MAX_SCREEN_WINDOWS=300
		;;

		net-wireless/cowpatty|app-i18n/tomoe|media-gfx/zbar|dev-libs/librep|x11-libs/rep-gtk|sys-fs/udev|mail-client/etpan-ng)
		# does not handle dependencies
		export MAKEOPTS="--jobs=1"
		;;

		media-plugins/gst-plugins-webrtc)
		export MAKEOPTS="--jobs=1"
		;;

		dev-libs/boost|xfce-extra/xfce4-wmdock)
		elog "Using fix: --jobs=N does NOT work with boost ebuild, using -jN"
		export MAKEOPTS="-j${JOBS}"
		;;

		sys-devel/gcc|sys-devel/binutils)
			for x in 1 2 3 4 5 6 7 8 9 10; do
			    PATH="${PATH//\/usr\/lib\/llvm\/${x}\/bin}"
			done
			elog "PATH: ${PATH}"
			elog "GCC: removed LLVM from PATH"
		;;

		dev-util/CastXML)
		PATH="/usr/lib/llvm/9/bin:$PATH"
		elog "Set CastXML to use LLVM 9"
		;;

		net-misc/janus)
		export MAKEOPTS="--jobs=1"
		;;

		sci-libs/openfoam)
		export WM_NCOMPPROCS=$JOBS
		export MAKEFLAGS="$MAKEOPTS"
		;;

		*)
		export MAKEOPTS="--jobs=$JOBS"
		export NINJAOPTS="-j ${JOBS}"
		;;
	esac
}





case $EBUILD_PHASE in
	setup)
	cJ_set_cflags
	cJ_set_jobs
	cJ_set_extra

	case $CATEGORY/$PN in
		dev-embedded/libftdi)
		libftdi_LIVE_REPO=git://developer.intra2net.com/libftdi
		;;

		sys-apps/paludis)
		# somehow the docs are generated including the commands... which is weird.
		unset CAVE_COMMANDS_PATH
		;;
	esac

	case $CATEGORY in
		# work around forced USE mutlilib flags and default profile
		# instead in the cross* repo, set profile=...
		cross-arm*)
		elog "Using ARM fixes"
		unset ABI
		unset MULTILIB_ABIS
		export LIBDIR_arm=lib
		export DEFAULT_ABI=arm
		#export MULTILIB_ABIS=arm
		;;

		cross-powerpc*)
		elog "Using PPC fixes"
		unset ABI
		unset MULTILIB_ABIS
		export LIBDIR_ppc=lib32
		export DEFAULT_ABI=ppc
		#export MULTILIB_ABIS=arm
		;;
	esac
	cJ_setup_kernel_dirs
	cJ_env

	# Backup ccache/distcc variables as portage eats them
	for var in ${!CCACHE_*}; do
		export cJ_${var}="$(eval echo \$${var})"
	done
	for var in ${!DISTCC_*}; do
		export cJ_${var}="$(eval echo \$${var})"
	done
	;;

	configure|compile)
	# Restore ccache/distcc variables
	for var in ${!cJ_CCACHE_*}; do
		export ${var#cJ_}="$(eval echo \$${var})"
	done
	for var in ${!cJ_DISTCC_*}; do
		export ${var#cJ_}="$(eval echo \$${var})"
	done
	cJ_env
	;;
esac

# crossdev non-standard versions...
#export I_PROMISE_TO_SUPPLY_PATCHES_WITH_BUGS=1
